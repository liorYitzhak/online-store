const express = require("express");
const router = express.Router();
const ProductsCartController = require("../controllers/productsCartsController");

router.post("/insertProduct", ProductsCartController.insertProduct);
router.get("/getProductsByCartId", ProductsCartController.getProductsByCartId);
router.post("/updateQuentity", ProductsCartController.updateQuentity);
router.get("/deleteProduct", ProductsCartController.deleteProduct);
router.get(
  "/deleteProductsByCartId",
  ProductsCartController.deleteProductsByCartId
);

module.exports = router;
