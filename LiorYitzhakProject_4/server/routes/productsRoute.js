const express = require("express");
const router = express.Router();
var multer = require("multer");
const ProductsController = require("../controllers/productsController");

/* here we set where the files save and set the name of file */
var storageObj = multer.diskStorage({
  destination: "./uploads",
  filename: function (req, file, cb) {
    cb(null, Date.now() + "_" + file.originalname);
  },
});
var upload = multer({ storage: storageObj });

router.get("/getProductsAmount", ProductsController.getProductsAmount);
router.get("/getProductByCategory", ProductsController.getProductByCategory);
router.get("/getProductByName", ProductsController.getProductByName);
router.get("/getAllCategoreis", ProductsController.getAllCategoreis);

router.post(
  "/insertProduct",
  upload.array("uploads[]", 12),
  ProductsController.insertProduct
);

router.post(
  "/updateProduct",
  upload.array("uploads[]", 12),
  ProductsController.updateProduct
);

module.exports = router;
