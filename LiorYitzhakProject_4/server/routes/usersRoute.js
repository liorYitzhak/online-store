const express = require("express");
const router = express.Router();
const UsersController = require("../controllers/usersController");

router.post("/insertUser", UsersController.insertUser);
router.post("/getUser", UsersController.getUser);
router.get("/checkUserPersonalId", UsersController.checkUserPersonalId);
router.get("/checkIfEmailExist", UsersController.checkIfEmailExist);

module.exports = router;
