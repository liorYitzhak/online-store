const express = require("express");
const router = express.Router();
const OrdersController = require("../controllers/ordersController");

router.post("/insertOrder", OrdersController.insertOrder);
router.get("/getOrdersAmount", OrdersController.getOrdersAmount);
router.get("/getOrdersAmountByDate", OrdersController.getOrdersAmountByDate);
router.get("/getOrdersByUserId", OrdersController.getOrdersByUserId);

module.exports = router;
