const express = require("express");
const router = express.Router();
const CartsController = require("../controllers/cartsController");

router.post("/insertCart", CartsController.insertCart);
router.get("/getCartsByUserId", CartsController.getCartsByUserId);
router.post("/updateCart", CartsController.updateCart);

module.exports = router;
