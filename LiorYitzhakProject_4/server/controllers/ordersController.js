const con = require("../utils/dataBase");
const Orders = require("../models/ordersModel");

/* here we get object from the client and insert to DB this a new order */
exports.insertOrder = async (req, res) => {
  let orderPlaceOk = false;
  await Orders.create(req.body)
    .then((result) => {
      orderPlaceOk = true;
      res.send(orderPlaceOk);
    })
    .catch((err) => {
      res.send(orderPlaceOk);
    });
};

/* here we get all orders from db and send to client the amount of all order */
exports.getOrdersAmount = async (req, res) => {
  await Orders.findAndCountAll()
    .then((result) => {
      res.send(String(result.count));
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get all orders in selected  date from db and send the amount to client */
exports.getOrdersAmountByDate = async (req, res) => {
  await Orders.findAndCountAll({ where: { deliveryDate: req.query.Date } })
    .then((result) => {
      res.send(String(result.count));
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get all orders of  specific user from db and send to client */
exports.getOrdersByUserId = async (req, res) => {
  await Orders.findAll({ where: { userId: req.query.userId } })
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};
