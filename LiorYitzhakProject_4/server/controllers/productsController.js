const con = require("../utils/dataBase");
const { Op } = require("sequelize");
const Products = require("../models/ProductsModel");
const Categoreis = require("../models/categoriesModel");

/* here we get object from the client and insert to DB this a new Product */
exports.insertProduct = async (req, res) => {
  let files = req.files;
  let newProductObj = {
    name: req.body.name,
    categoryId: req.body.categoryId,
    price: req.body.price,
    image: files[0].filename,
  };
  await Products.create(newProductObj)
    .then((result) => {
      console.log(result);
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
  res.send(files[0]);
};

/* here we get object  from client update  the product ditals and sent to db  */
exports.updateProduct = async (req, res) => {
  let files;
  if (req.files[0] != undefined) {
    files = req.files[0].filename;
  } else {
    files = req.body.files;
  }
  await Products.update(
    {
      name: req.body.name,
      categoryId: req.body.categoryId,
      price: req.body.price,
      image: files,
    },
    {
      where: {
        id: req.body.id,
      },
    }
  )
    .then((result) => {
      console.log(result);

      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get all Products from db and send to client the amount of all order */
exports.getProductsAmount = async (req, res) => {
  await Products.findAndCountAll()
    .then((result) => {
      res.send(String(result.count));
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get all Products of specific category (by category id from the client ) from dband send to client */
exports.getProductByCategory = async (req, res) => {
  await Products.findAll({ where: { categoryId: req.query.id } })
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get from client a string after serch and we get from db only products iclude the string and send to client */
exports.getProductByName = async (req, res) => {
  await Products.findAll({
    where: { name: { [Op.startsWith]: [req.query.name] } },
  })
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get all Categoreis from db and send to client */
exports.getAllCategoreis = async (req, res) => {
  await Categoreis.findAll()
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};
