const con = require("../utils/dataBase");
const Carts = require("../models/cartsModel");

/* here we get all Carts of specific user id (by user id from the client ) from db and send to client */
exports.getCartsByUserId = async (req, res) => {
  await Carts.findAll({ where: { userId: req.query.userId } })
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get object from the client and insert to DB this as new cart */
exports.insertCart = async (req, res) => {
  let cartObj = {
    userId: req.body.userId,
    isOpen: req.body.isOpen,
  };
  await Carts.create(cartObj)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get object  from client and  update  the db if the cart  isOpen or not  */
exports.updateCart = async (req, res) => {
  await Carts.update(
    {
      isOpen: req.body.isOpen,
    },
    {
      where: {
        id: req.body.id,
      },
    }
  )
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};
