const con = require("../utils/dataBase");
const ProductsCart = require("../models/productsCartModel");
const Products = require("../models/ProductsModel");

/* here we get object from the client and insert to DB this as new Product Cart */
exports.insertProduct = async (req, res) => {
  await ProductsCart.create(req.body)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get all Products of specific Cart  (by Cart id  ) from db and send to client */
exports.getProductsByCartId = async (req, res) => {
  await ProductsCart.findAll({
    where: { cartId: req.query.cartId },
    include: [
      {
        model: Products,
      },
    ],
  })
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get object  from client and  update quentity of product  */
exports.updateQuentity = async (req, res) => {
  await ProductsCart.update(
    {
      quentity: req.body.quentity,
      price: req.body.price,
    },
    {
      where: {
        id: req.body.id,
      },
    }
  )
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};

/* Here we delete one product from the product cart */
exports.deleteProduct = async (req, res) => {
  await ProductsCart.destroy({ where: { id: req.query.id } })
    .then((result) => {
      res.send("product delete ok");
    })
    .catch((err) => {
      res.send(err);
    });
};

/* Here we delete all products from the cart (by cart ID ) */
exports.deleteProductsByCartId = async (req, res) => {
  await ProductsCart.destroy({ where: { cartId: req.query.id } })
    .then((result) => {
      res.send("products cart delete ok");
    })
    .catch((err) => {
      res.send(err);
    });
};
