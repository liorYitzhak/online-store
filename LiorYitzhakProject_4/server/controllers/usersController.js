const con = require("../utils/dataBase");
const Users = require("../models/usersModel");
const bcrypt = require("bcrypt");
const saltRounds = 10;

/* here we get object from the client to add a new user and  send to DB as new user */
exports.insertUser = async (req, res) => {
  let obj = req.body;
  await bcrypt.hash(obj.password, saltRounds, async function (err, hash) {
    obj.password = hash;
    await Users.create(obj)
      .then((result) => {
        res.send(result);
      })
      .catch((err) => {
        res.send("error:" + err);
      });
  });
};

/* we do check if the the Personal Id  exsist  */
exports.checkUserPersonalId = async (req, res) => {
  await Users.count({ where: { personalId: req.query.personalId } })
    .then((result) => {
      res.send(String(result));
    })
    .catch((err) => {
      res.send(err);
    });
};

/* we do check if the mail exsist  */
exports.checkIfEmailExist = async (req, res) => {
  await Users.count({ where: { mail: req.query.mail } })
    .then((result) => {
      res.send(String(result));
    })
    .catch((err) => {
      res.send(err);
    });
};

/* here we get mail and password from the client and do check if is correct in DB
 If this is correct send back the user information, if not send an error  */
exports.getUser = async (req, res) => {
  let user = null;
  let crrectPassword = false;
  await Users.findOne({
    where: { mail: req.body.mail },
  })
    .then((result) => {
      if (result != null) {
        user = result;
        bcrypt.compare(
          req.body.password,
          user.password,
          function (err, result) {
            crrectPassword = result;
            if (crrectPassword) {
              res.send(user);
            } else {
              user = null;
              res.send(user);
            }
          }
        );
      } else {
        res.send(result);
      }
    })
    .catch((err) => {
      res.send(err);
    });
};
