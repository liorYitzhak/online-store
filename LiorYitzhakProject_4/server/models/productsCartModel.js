const Sequelize = require("sequelize");
const sequelizeDBConnection = require("../utils/dataBase");

const ProductsCart = sequelizeDBConnection.define("ProductsCart", {
  id: {
    type: Sequelize.INTEGER(11),
    autoIncrement: true,
    primaryKey: true,
  },
  productId: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
  },
  quentity: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
  },
  price: {
    type: Sequelize.DECIMAL(11, 2),
    allowNull: false,
  },
  cartId: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
  },
});
module.exports = ProductsCart;
