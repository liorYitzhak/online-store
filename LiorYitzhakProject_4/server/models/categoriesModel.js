const Sequelize = require("sequelize");
const sequelizeDBConnection = require("../utils/dataBase");

const Categories = sequelizeDBConnection.define("categories", {
  id: {
    type: Sequelize.INTEGER(11),
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});
module.exports = Categories;
