const Sequelize = require("sequelize");
const sequelizeDBConnection = require("../utils/dataBase");

const Orders = sequelizeDBConnection.define("orders", {
  id: {
    type: Sequelize.INTEGER(11),
    autoIncrement: true,
    primaryKey: true,
  },
  userId: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
  },
  cartId: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
  },
  totalPrice: {
    type: Sequelize.DECIMAL(11, 2),
    allowNull: false,
  },
  city: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  street: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  deliveryDate: {
    type: Sequelize.DATEONLY,
    allowNull: false,
  },
  creditCard: {
    type: Sequelize.INTEGER(4),
    allowNull: false,
  },
});
module.exports = Orders;
