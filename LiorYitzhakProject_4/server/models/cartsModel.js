const Sequelize = require("sequelize");
const sequelizeDBConnection = require("../utils/dataBase");

const Carts = sequelizeDBConnection.define("carts", {
  id: {
    type: Sequelize.INTEGER(11),
    autoIncrement: true,
    primaryKey: true,
  },
  userId: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
  },
  isOpen: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
  },
});
module.exports = Carts;
