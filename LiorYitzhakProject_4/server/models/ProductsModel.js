const Sequelize = require("sequelize");
const sequelizeDBConnection = require("../utils/dataBase");

const Products = sequelizeDBConnection.define("products", {
  id: {
    type: Sequelize.INTEGER(11),
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  categoryId: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
  },
  price: {
    type: Sequelize.DECIMAL(11, 2),
    allowNull: false,
  },
  image: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});
module.exports = Products;
