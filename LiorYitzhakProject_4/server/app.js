const express = require("express");
const app = express();
var multer = require("multer");
var path = require("path");
const cors = require("cors");
const bodyParser = require("body-parser");
const Sequelize = require("sequelize");
const sequelize = require("./utils/dataBase");

const Users = require("./models/usersModel");
const Categories = require("./models/categoriesModel");
const Carts = require("./models/cartsModel");
const Orders = require("./models/ordersModel");
const Products = require("./models/ProductsModel");
const ProductsCart = require("./models/productsCartModel");

/* this line is for the image upload */

app.use("/uploads", express.static(path.join(__dirname, "uploads")));

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
var corsOptions = {
  origin: "*",
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

/* from here we do all the conction between the teble by foreing key */
Users.hasMany(Orders, { foreignKey: "userId" });
Orders.belongsTo(Users, { foreignKey: "userId" });

Users.hasMany(Carts, { foreignKey: "userId" });
Carts.belongsTo(Users, { foreignKey: "userId" });

Categories.hasMany(Products, { foreignKey: "categoryId" });
Products.belongsTo(Categories, { foreignKey: "categoryId" });

Products.hasMany(ProductsCart, { foreignKey: "productId" });
ProductsCart.belongsTo(Products, { foreignKey: "productId" });

Carts.hasMany(ProductsCart, { foreignKey: "cartId" });
ProductsCart.belongsTo(Carts, { foreignKey: "cartId" });

Carts.hasMany(Orders, { foreignKey: "cartId" });
Orders.belongsTo(Carts, { foreignKey: "cartId" });

/* Here we define all the paths of the app addresses */
const UsersRoute = require("./routes/usersRoute");
app.use("/users", UsersRoute);

const OrdersRoute = require("./routes/ordersRoute");
app.use("/orders", OrdersRoute);

const ProductsRoute = require("./routes/productsRoute");
app.use("/products", ProductsRoute);

const CartsRoute = require("./routes/cartsRoute");
app.use("/carts", CartsRoute);

const CategoriesRoute = require("./routes/productsRoute");
app.use("/categories", CategoriesRoute);

const ProductsCartsRoute = require("./routes/prodcutsCartRoute");
app.use("/productsCarts", ProductsCartsRoute);

app.use((req, res) => {
  res.send("Page Not Found");
});

sequelize
  .sync()
  .then((result) => {
    app.listen(4000);
  })
  .catch((err) => {
    console.log("error", "ERRR " + JSON.stringify(err));
  });
