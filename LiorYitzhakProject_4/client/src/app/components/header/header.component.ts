import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from 'src/app/services/cart.service';
import { ProductsService } from 'src/app/services/products.service';
import { UserService } from 'src/app/services/user.service';
import { Category } from '../../modules/product';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(
    public route: Router,
    public userService: UserService,
    public prodctService: ProductsService,
    public cartService: CartService
  ) {}

  /* here we do log out for the user */
  logOut = () => {
    this.userService.userLogOut();
    this.route.navigate(['/login']);
    this.userService.userInHomePage = false;
    this.cartService.cartStatus = '';
  };
  /* here we get the pic category from client and send to server for get the category prodcuts */
  selcetCategory = (category: Category) => {
    scroll(0, 0);
    this.prodctService.currentCategory = category;
    this.prodctService.getProductsByCategory(category.id);
    this.prodctService.showCategoryTitle = true;
    this.prodctService.searchBox = '';
  };

  /* here it is for by the time client click on the logo we go to the login page */
  goToLogPage = () => {
    this.route.navigate(['/login']);
  };
  ngOnInit(): void {}
}
