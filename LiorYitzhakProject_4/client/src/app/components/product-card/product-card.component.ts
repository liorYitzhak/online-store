import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { UserService } from 'src/app/services/user.service';
import { Product } from '../../modules/product';
import { ProductForm } from '../../modules/form';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css'],
})
export class ProductCardComponent implements OnInit {
  @Output() showFormFun = new EventEmitter();

  constructor(
    public productService: ProductsService,
    public userService: UserService,
    public cartService: CartService
  ) {}

  /* here we collect data from product an bild prodct form tp send home page */
  editProduct = (product: Product) => {
    let productToEdit: ProductForm = new ProductForm();
    productToEdit.id = product.id;
    productToEdit.categoryId = product.categoryId;
    productToEdit.name = product.name;
    productToEdit.price = product.price;
    this.productService.newOrEditProdcut = productToEdit;
    this.productService.editProductImage = product.image;
    this.showFormFun.emit(productToEdit);
  };

  /* here we change the qty of the product card it is for the add to cart qty */
  changeQty = (status: string, id: number) => {
    let counter: any = document.getElementById(String(id));
    let counterValue = Number(counter.value);
    if (status == 'plus') (counterValue += 1), (counter.value = counterValue);
    if (status == 'minus') {
      if (counterValue > 1) (counterValue -= 1), (counter.value = counterValue);
    }
  };

  /* here we cheack if the product exisist in car we do update and if dont exsisit we insert */
  addOrEditProductCart = async (product: Product) => {
    let counter: any = document.getElementById(String(product.id));
    let qty = counter.value;
    let item = this.cartService._liveCart.find(
      (liveProduct) => liveProduct.productId === product.id
    );
    item == undefined
      ? await this.cartService.insertProductToProductsCart(product, qty)
      : await this.cartService.editProductCart(product, qty);
    this.cartService.calculateTotalPrice(false);
  };

  ngOnInit(): void {}
}
