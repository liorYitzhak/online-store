import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { ApiService } from 'src/app/services/api.service';
import { UserService } from 'src/app/services/user.service';
import { ValidateService } from 'src/app/services/validate.service';
import { RegistrationStepOne, RegistrationStepTwo } from '../../modules/form';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  currentStep: number = 1;
  stepOne: RegistrationStepOne = new RegistrationStepOne();
  stepTwo: RegistrationStepTwo = new RegistrationStepTwo();
  constructor(
    public api: ApiService,
    public validate: ValidateService,
    public userService: UserService,
    public alert: AlertService,
    public route: Router
  ) {}

  /* here we do validate to step one registration  and if all ok we go to next step */
  nextStep = async () => {
    let isValidate = await this.validate.checkRegisterFormStepOne(this.stepOne);
    if (isValidate) {
      this.currentStep = 2;
    }
  };

  /* here we do validate to step Two registration  and if all ok we send to server as a new user */
  sendForm = async () => {
    let isOk: Boolean = false;
    let isValidate = await this.validate.checkRegisterFormStepTwo(this.stepTwo);
    if (isValidate) {
      isOk = await this.userService.sendToServerNewUser(
        this.stepOne,
        this.stepTwo
      );
    }
    if (isOk == true) {
      this.userService.setUserToLoacalStorage(this.userService._currentUser);
      this.route.navigate(['/login']);
      this.alert.successMessage(
        'You have successfully registered! You will immediately go to the home page'
      );
    } else
      this.alert.serverProblem('Registration failed. Please try again later');
  };

  /* here we go back to step one register its came from click button */
  goBackToStepOne = () => {
    this.currentStep = 1;
  };
  ngOnInit(): void {}
}
