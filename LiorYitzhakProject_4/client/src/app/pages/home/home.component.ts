import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { CartService } from 'src/app/services/cart.service';
import { ProductsService } from 'src/app/services/products.service';
import { UserService } from 'src/app/services/user.service';
import { ValidateService } from 'src/app/services/validate.service';
import { ProductForm } from '../../modules/form';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  isCartSideBarOpen: boolean = true;
  fadeClassAnimation: string = '';
  btnAnimation: string = '';
  showAddOrEditForm: boolean = false;
  cardsWidth: string = '';
  isEditProdct: boolean = false;
  showAddNewProductInAdminCart: boolean = true;
  constructor(
    public validate: ValidateService,
    public productService: ProductsService,
    public userService: UserService,
    public alert: AlertService,
    public cartService: CartService,
    public route: Router
  ) {
    this.cartService.cartTotalPrice = 0;
    this.productService.getCategories();
    this.userService.userInHomePage = true;
    this.userService.cheackUserInLocalStorage();
    this.cartService.getUserCart(this.userService._currentUser.id);
    this.userService.setIfIsAdmin();
    this.validate.pageDirection();
  }
  /* here we get the image file from user and update the newProdcut object  */
  getFile(event: any) {
    this.productService.newOrEditProdcut.image = event.target.files[0];
  }

  /* here we call to insert product function from product service for insert a new product to the server */
  saveProduct = async () => {
    let isOk: Boolean = false;
    let isValidate = this.validate.checkProductForm(
      this.productService.newOrEditProdcut,
      this.isEditProdct
    );
    if (isValidate) {
      this.isEditProdct
        ? (isOk = await this.productService.editProduct())
        : (isOk = await this.productService.insertNewProduct());
      if (isOk) {
        this.alert.successMessage('The Product successfully saved'),
          this.productService.getProductsByCategory(
            this.productService.currentCategory.id
          );
      } else this.alert.serverProblem('The Product not saved');
    }
    this.showAddOrEditForm = false;
    this.showAddNewProductInAdminCart = true;
  };

  /* here we add or remove class for hide or show the cart */
  closeOrOpenCart = () => {
    this.isCartSideBarOpen == true
      ? (this.isCartSideBarOpen = false)
      : (this.isCartSideBarOpen = true);
    if (this.isCartSideBarOpen == false) {
      this.fadeClassAnimation = 'cartFadeOut';
      this.btnAnimation = 'btnOut';
      this.cardsWidth = 'cardsAllPage';
      this.productService.prodctsRowClass = 'col-xl-3 col-md-4';
    } else {
      this.fadeClassAnimation = 'cartFadeIn';
      this.btnAnimation = 'btnIn';
      this.cardsWidth = '';
      this.productService.prodctsRowClass = 'col-xl-4 col-md-6';
    }
  };

  /* this is a on click function to add or edit product  */
  showForm = (isEdit: boolean) => {
    this.showAddOrEditForm = true;
    this.isEditProdct = isEdit;
    if (isEdit == false)
      this.productService.newOrEditProdcut = new ProductForm();
    this.showAddNewProductInAdminCart = false;
  };

  /* here is for the user finsh the order and wont move to the place order  */
  goToCheckoutPage = () => {
    this.route.navigate(['/order']);
  };

  ngOnInit(): void {}
}
