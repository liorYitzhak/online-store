import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ExportFileService } from '../../services/export-file.service';
import { Router } from '@angular/router';
import { CartService } from 'src/app/services/cart.service';
import { OrdersService } from 'src/app/services/orders.service';
import { UserService } from 'src/app/services/user.service';
import { LiveProductCart } from '../../modules/prodctCart';
import { ValidateService } from 'src/app/services/validate.service';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
})
export class OrderComponent implements OnInit {
  @ViewChild('receiptTable')
  receiptTable!: ElementRef;
  searchBox: string = '';
  constructor(
    public cartService: CartService,
    public userService: UserService,
    public orderService: OrdersService,
    public validate: ValidateService,
    public alert: AlertService,
    public route: Router,
    private exportService: ExportFileService
  ) {
    this.userService.cheackUserInLocalStorage();
    this.cartService.getUserCart(this.userService._currentUser.id);
    this.validate.pageDirection(true);
  }

  /* here we do on products receipt and show only serch products */
  searchProductOnOrder = (value: string) => {
    this.cartService.filterCartProducts = this.cartService._liveCart.filter(
      (product: LiveProductCart) => {
        return product.name.toLowerCase().includes(value.toLowerCase());
      }
    );
  };

  /* here we do validate to the order form and if is ok we send to the server as a new order */
  sendOrder = async () => {
    let isOk = await this.validate.checkNewOrderForm(
      this.orderService.orderForm
    );
    if (isOk) {
      let sendOk = this.orderService.sendNewOrder();
      if (sendOk) {
        this.cartService.markCartAsClose();
        this.exportService.receiptTable = this.receiptTable;
        this.alert.orderPlacedOk();
      }
    }
  };

  /* here we do auto fill to the city and street fild wehen the user do dblclick on the fild  */
  autoFill = (text: string) => {
    if (text == 'city')
      this.orderService.orderForm.city = this.userService._currentUser.city;
    if (text == 'street')
      this.orderService.orderForm.street = this.userService._currentUser.street;
  };

  /* it is for back to store btn */
  goToHomePage = () => {
    this.route.navigate(['/home']);
  };
  ngOnInit(): void {}
}
