import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { CartService } from 'src/app/services/cart.service';
import { OrdersService } from 'src/app/services/orders.service';
import { UserService } from 'src/app/services/user.service';
import { LogIn } from '../../modules/form';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  logIn: LogIn = new LogIn();
  productsAmount: string = '';

  constructor(
    public userService: UserService,
    public api: ApiService,
    public cartService: CartService,
    public orderService: OrdersService,
    public route: Router
  ) {
    this.userService.userInHomePage = false;
    this.getProductsAmount();
    this.userService.cheackUserInLocalStorage();
    this.setHowIsLogIn();
  }

  /* here wo call to server and get the number of orders we have in db from this store */
  getProductsAmount = async () => {
    this.productsAmount = (await this.api.getApiRequest(
      'products/getProductsAmount'
    )) as string;
  };

  /* here we bild the user object from the log in form and send to function in user service */
  loginBtn = async () => {
    await this.userService.userLogIn(this.logIn);
    this.setHowIsLogIn();
  };

  /* here we set how is log in  */
  setHowIsLogIn = async () => {
    if (this.userService._currentUser.lastName != '') {
      this.userService.afterLogIn = true;
      this.userService.logInBtnValue = 'login With another account';
      this.logIn = new LogIn();
      await this.cartService.getUserCart(this.userService._currentUser.id);
      await this.orderService.getAllOrders();
      await this.orderService.findLastOrder();
    } else this.userService.afterLogIn = false;
    if (this.userService._currentUser.role == 1) this.route.navigate(['/home']);
  };

  /* here we do logout user and reset all the change we do after the log in */
  logoutBtn = () => {
    this.userService.userLogOut();

    this.logIn = new LogIn();
    this.cartService.cartStatus = '';
  };

  /* here we do route to the home page after the client click on button to start shopping */
  goToHomePage = () => {
    this.route.navigate(['/home']);
  };

  /* here we route the page to register page (it is came from on click btn) */
  goRegisterPage = () => {
    this.route.navigate(['/registration']);
  };

  ngOnInit(): void {}
}
