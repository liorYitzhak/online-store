export class Order {
  id: number = 0;
  userId: number = 0;
  cartId: number = 0;
  totalPrice: number = 0;
  city: string = '';
  street: string = '';
  deliveryDate: Date = new Date();
  creditCard: number = 0;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
}
