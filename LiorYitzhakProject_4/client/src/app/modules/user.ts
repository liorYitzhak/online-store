export class User {
  id: number = 0;
  firstName: string = '';
  lastName: string = '';
  mail: string = '';
  password: string = '';
  personalId: number = 0;
  city: string = '';
  street: string = '';
  role: number = 0;
  createdAt?: Date = new Date();
  updatedAt?: Date = new Date();
}
