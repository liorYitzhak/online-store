import { Product } from '../modules/product';

export class ProductCart {
  id: number = 0;
  productId: number = 0;
  product: Product = new Product();
  quentity: number = 0;
  price: number = 0;
  cartId: number = 0;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
}
export class LiveProductCart {
  id: number = 0;
  productId: number = 0;
  quentity: number = 0;
  price: number = 0;
  image: string = '';
  name: string = '';
}

export class EditProduct {
  quentity: number = 0;
  id: number = 0;
  price: number = 0;
}
