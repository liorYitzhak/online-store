export class LogIn {
  mail: string = '';
  password: string = '';
}

export class RegistrationStepOne {
  personalId: any;
  mail: string = '';
  password: string = '';
  confirmPassword: string = '';
}

export class RegistrationStepTwo {
  city: string = '';
  street: string = '';
  firstName: string = '';
  lastName: string = '';
}

export class ProductForm {
  id?: number = 0;
  name: string = '';
  categoryId: number = 0;
  price: any;
  image: any = null;
}

export class NewProductErr {
  name: string = '';
  category: string = '';
  price: string = '';
  imageErr: string = '';
}

export class PlaceOrder {
  city: string = '';
  street: string = '';
  deliveryDate: Date = new Date();
  creditCard: any;
}

export class PlaceOrderErr {
  city: string = '';
  street: string = '';
  deliveryDate: string = '';
  creditCard: string = '';
}
