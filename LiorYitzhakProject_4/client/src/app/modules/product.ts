export class Product {
  id: number = 0;
  name: string = '';
  categoryId: number = 0;
  price: number = 0;
  image: string = '';
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
}

export class Category {
  id: number = 0;
  name: string = '';
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
}
