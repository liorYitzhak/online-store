export class Cart {
  id: number = 0;
  userId: number = 0;
  isOpen: boolean = false;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
}
