import { findLast } from '@angular/compiler/src/directive_resolver';
import { Pipe, PipeTransform } from '@angular/core';

/* this pipe is for the Search the receipt and mark the text in yellow */
@Pipe({
  name: 'markText',
})
export class MarkTextPipe implements PipeTransform {
  /* here we get text and return the text with a capital the first letter */
  capitalizeFirstLetter = (word: string) => {
    return word.charAt(0).toUpperCase() + word.slice(1);
  };

  transform(value: any, ...args: any[]): any {
    value = value.toLowerCase();
    if (!args) {
      return value;
    }
    let isExists = args[1].toLowerCase().includes(args[0].toLowerCase());
    if (isExists) {
      if (args[0] != '') {
        for (let text of args) {
          var reText: any = new RegExp(text);
          value = value.replace(reText, '<mark>' + text + '</mark>');
        }
      }
    }
    value = value.split(' ').map(this.capitalizeFirstLetter).join(' ');
    return value;
  }
}
