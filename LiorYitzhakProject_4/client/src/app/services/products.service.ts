import { Injectable } from '@angular/core';
import { Product } from '../modules/product';
import { ApiService } from './api.service';
import { Category } from '../modules/product';
import { ProductForm } from '../modules/form';
import { Data } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  _products: Array<Product> = new Array();
  _categoreis: Array<Category> = new Array();
  newOrEditProdcut: ProductForm = new ProductForm();
  prodctsRowClass: string = 'col-xl-4 col-md-6';
  editProductImage: string = '';
  currentCategory: Category = new Category();
  searchBox: string = '';
  serchTitle: string = '';
  showCategoryTitle: boolean = true;

  constructor(public api: ApiService) {
    this.getCategories();
  }

  /* here we get all the categories from db */
  getCategories = async () => {
    this._categoreis = (await this.api.getApiRequest(
      'categories/getAllCategoreis'
    )) as Array<Category>;
    this.currentCategory = this._categoreis[0];
    this.getProductsByCategory(this._categoreis[0].id);
  };

  /* here we biled a form data object from the form and send to server as a new product */
  insertNewProduct = async () => {
    let uploadSuccessfully: boolean = false;
    const formData: Data = new FormData();
    formData.append(
      'uploads[]',
      this.newOrEditProdcut.image,
      this.newOrEditProdcut.image['name']
    );
    formData.append('name', this.newOrEditProdcut.name);
    formData.append('categoryId', this.newOrEditProdcut.categoryId);
    formData.append('price', this.newOrEditProdcut.price);

    let theNewProduct: any = await this.api.postApiRequest(
      'products/insertProduct',
      formData
    );
    this.newOrEditProdcut = new ProductForm();
    theNewProduct.name
      ? (uploadSuccessfully = true)
      : (uploadSuccessfully = false);
    return uploadSuccessfully;
  };

  /* here we bild a data object for edit product to db */
  editProduct = async () => {
    let image: string = '';
    let updateSuccessfully: boolean = false;
    const formData: Data = new FormData();
    if (this.newOrEditProdcut.image != null) {
      formData.append(
        'uploads[]',
        this.newOrEditProdcut.image,
        this.newOrEditProdcut.image['name']
      );
    } else {
      image = this.editProductImage;
      formData.append('files', image);
    }
    formData.append('name', this.newOrEditProdcut.name);
    formData.append('categoryId', this.newOrEditProdcut.categoryId);
    formData.append('price', this.newOrEditProdcut.price);
    formData.append('id', this.newOrEditProdcut.id);

    let resulte = await this.api.postApiRequest(
      'products/updateProduct',
      formData
    );
    updateSuccessfully = resulte as boolean;
    this.newOrEditProdcut = new ProductForm();
    return updateSuccessfully;
  };

  /* here we get all products of Selected category */
  getProductsByCategory = async (id: number) => {
    this._products = (await this.api.getApiRequest(
      `products/getProductByCategory?id=${id}`
    )) as Array<Product>;
  };

  /* here we do search from all product by name */
  searchProduct = async () => {
    if (this.searchBox == '') {
      this.serchTitle = '';
      this.showCategoryTitle = true;
      this.getProductsByCategory(this.currentCategory.id);
    } else {
      this.serchTitle = `"${this.searchBox}"`;
      this.showCategoryTitle = false;
      let resulte = await this.api.getApiRequest(
        `products/getProductByName?name=${this.searchBox}`
      );
      this._products = resulte as Array<Product>;
      if (this._products.length == 0)
        this.serchTitle = `" ${this.searchBox}" is Not found`;
    }
  };
}
