import { Injectable } from '@angular/core';
import { User } from '../modules/user';
import { ApiService } from './api.service';
import { RegistrationStepOne, RegistrationStepTwo } from '../modules/form';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  _currentUser: User = new User();
  newUser: User = new User();
  incorrect: string = '';
  afterLogIn: boolean = false;
  logInBtnValue: string = 'login';
  userInHomePage: boolean = false;
  isAdmin: boolean = false;

  constructor(public api: ApiService) {}

  /* here we get to object from register page and bild a new user object to send to server as a new user
  finlay we do check if we get good resulte we send back true if we get error from server we send back false
  */
  sendToServerNewUser = async (
    stepOne: RegistrationStepOne,
    steoTwo: RegistrationStepTwo
  ) => {
    let status: boolean = false;
    this.newUser.personalId = stepOne.personalId;
    this.newUser.mail = stepOne.mail;
    this.newUser.password = stepOne.password;
    this.newUser.city = steoTwo.city;
    this.newUser.street = steoTwo.street;
    this.newUser.firstName = steoTwo.firstName;
    this.newUser.lastName = steoTwo.lastName;
    let sendUser: any = await this.api.postApiRequest(
      `users/insertUser`,
      this.newUser
    );
    if (sendUser.city != '') {
      status = true;
      this._currentUser = sendUser;
    }
    if (sendUser == 'erroe server') status = false;
    return status;
  };

  /* here we set the uesr in the local storage */
  setUserToLoacalStorage = (user: User) => {
    localStorage.setItem('currentUser', JSON.stringify(user));
  };

  /* here we get the user datiles from local storage and  set the _currentUser */
  cheackUserInLocalStorage = () => {
    let userFromLocalStorage = localStorage.getItem('currentUser');
    if (userFromLocalStorage != null)
      this._currentUser = JSON.parse(userFromLocalStorage);
  };

  /* here we get a user object (email and password) from the login page and call to server
   if we get null the email or passwors is incorrect else we get the user info  */
  userLogIn = async (userObject: object) => {
    this.incorrect = '';
    let user = await this.api.postApiRequest('users/getUser', userObject);
    if (user != null) {
      this._currentUser = user as User;
      this.setUserToLoacalStorage(this._currentUser);
    } else this.incorrect = 'Incorrect email or password';
  };

  /* here we log out the current user and rest all the change we do after the log in */
  userLogOut = () => {
    this._currentUser = new User();

    this.afterLogIn = false;
    this.logInBtnValue = 'login';
    localStorage.removeItem('currentUser');
  };

  /* here we do check if is admin for update the html */
  setIfIsAdmin = () => {
    this.isAdmin = false;
    if (this._currentUser.role == 1) {
      this.isAdmin = true;
    }
  };
}
