import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import {
  RegistrationStepOne,
  RegistrationStepTwo,
  NewProductErr,
  ProductForm,
  PlaceOrder,
  PlaceOrderErr,
} from '../modules/form';
import { Router } from '@angular/router';
import { UserService } from './user.service';
import { CartService } from './cart.service';

@Injectable({
  providedIn: 'root',
})
export class ValidateService {
  isErrorStepOne: RegistrationStepOne = new RegistrationStepOne();
  isErrorStepTwo: RegistrationStepTwo = new RegistrationStepTwo();
  isErrorNewProduct: NewProductErr = new NewProductErr();
  isErrorPlaceOrder = new PlaceOrderErr();
  constructor(
    public api: ApiService,
    public route: Router,
    public userService: UserService,
    public cartService: CartService
  ) {}

  /* here we do check if the user is connect and if not we send hime back to login page */
  pageDirection = (isOrderPage?: boolean) => {
    if (this.userService._currentUser.firstName == '')
      this.route.navigate(['/login']);
    if (isOrderPage) {
      if (this.cartService.cartTotalPrice == 0) this.route.navigate(['/home']);
    }
  };

  /* this fun is for validate the input credit card  */
  creditCardValidation = (creditCradNum: any) => {
    var regEx = /^((4\d{3})|(5[1-5]\d{2}))(-?|\040?)(\d{4}(-?|\040?)){3}|^(3[4,7]\d{2})(-?|\040?)\d{6}(-?|\040?)\d{5}/;
    if (String(creditCradNum).match(regEx)) {
      return true;
    } else {
      return false;
    }
  };

  /* this fun is for validate the input email address  */
  ValidateEmail(mail: string) {
    if (
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
        mail
      )
    ) {
      return true;
    }
    return false;
  }

  /* here we do all the valedate to step one register and insert the error text to  any field that has not been valedate 
  we get the object from registration.component */
  checkRegisterFormStepOne = async (registerObject: RegistrationStepOne) => {
    let flag: boolean = true;
    this.isErrorStepOne = new RegistrationStepOne();

    if (registerObject.personalId.toString().length != 9) {
      this.isErrorStepOne.personalId =
        ' Please insert a full ID (9 Characters) ';
      flag = false;
    } else {
      let personalIdExist = await this.api.getApiRequest(
        `users/checkUserPersonalId?personalId=${registerObject.personalId}`
      );
      if (personalIdExist == 1) {
        this.isErrorStepOne.personalId = '  ID is olrady Existing';
        flag = false;
      }
    }

    let mail: boolean = this.ValidateEmail(registerObject.mail);
    if (mail == false) {
      this.isErrorStepOne.mail = ' invalid email! Please enter a valid email ';
      flag = false;
    } else {
      let mailExsit = await this.api.getApiRequest(
        `users/checkIfEmailExist?mail=${registerObject.mail}`
      );
      if (mailExsit == 1) {
        this.isErrorStepOne.mail = '  Email address is olrady Existing';
        flag = false;
      }
    }

    if (registerObject.password.length < 6) {
      this.isErrorStepOne.password =
        ' Please insert a full password (At least 6 characters) ';
      flag = false;
    }

    if (registerObject.confirmPassword != registerObject.password) {
      this.isErrorStepOne.confirmPassword = ' Verify password does not match ';
      flag = false;
    }

    return flag;
  };

  /* here we do all the valedate to step Two register and insert the error text to  any field that has not been valedate 
  we get the object from registration.component */
  checkRegisterFormStepTwo = (registerObject: RegistrationStepTwo) => {
    let flag: boolean = true;
    this.isErrorStepTwo = new RegistrationStepTwo();

    if (registerObject.city == '') {
      this.isErrorStepTwo.city = ' Please select a city from the list ';
      flag = false;
    }

    if (registerObject.street.length < 2) {
      this.isErrorStepTwo.street = ' Please enter a full street name ';
      flag = false;
    }

    if (registerObject.firstName.length < 2) {
      this.isErrorStepTwo.firstName = ' Please enter a full first name ';
      flag = false;
    }

    if (registerObject.lastName.length < 2) {
      this.isErrorStepTwo.lastName = ' Please enter a full last name ';
      flag = false;
    }

    return flag;
  };

  /* here we do validate for admin add a new product */
  checkProductForm = (newProduct: ProductForm, isEditProdct: boolean) => {
    let flag: boolean = true;
    this.isErrorNewProduct = new NewProductErr();
    if (newProduct.categoryId == 0) {
      this.isErrorNewProduct.category =
        ' Please select a category from the list ';
      flag = false;
    }

    if (newProduct.name.length < 2) {
      this.isErrorNewProduct.name = ' Please enter a full product name ';
      flag = false;
    }

    if (newProduct.price == 0 || newProduct.price == null) {
      this.isErrorNewProduct.price = ' Please enter a product price ';
      flag = false;
    }

    if (isEditProdct == false) {
      if (newProduct.image == null) {
        this.isErrorNewProduct.imageErr = ' Please upload a product image ';
        flag = false;
      }
    }
    return flag;
  };

  /* here we do validate for place a new order */
  checkNewOrderForm = async (newOrder: PlaceOrder) => {
    let flag: boolean = true;
    /* this is a REGEX for the credit card */
    let creditCardOk = this.creditCardValidation(newOrder.creditCard);

    let orderDateAmount: number = (await this.api.getApiRequest(
      'orders/getOrdersAmountByDate?Date=' + newOrder.deliveryDate
    )) as number;
    this.isErrorPlaceOrder = new PlaceOrderErr();
    if (newOrder.city.length < 2) {
      this.isErrorPlaceOrder.city = ' Please enter a city for delivery ';
      flag = false;
    }

    if (newOrder.street.length < 2) {
      this.isErrorPlaceOrder.street = ' Please enter a street for delivery ';
      flag = false;
    }
    if (orderDateAmount > 2) {
      this.isErrorPlaceOrder.deliveryDate =
        'There are no deliveries available on the date you selected Please select another date';
      flag = false;
    }

    if (creditCardOk == false) {
      this.isErrorPlaceOrder.creditCard =
        ' Incorrect card number Please enter a valid number ';
      flag = false;
    }

    return flag;
  };
}
