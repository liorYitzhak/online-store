import { Injectable } from '@angular/core';
import { Order } from '../modules/order';
import { ApiService } from './api.service';
import { CartService } from './cart.service';
import { UserService } from './user.service';
import { PlaceOrder } from '../modules/form';
import { Router } from '@angular/router';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  allOrders: Array<Order> = new Array();
  lastOrder: Order = new Order();
  ordersAmount: string = '';
  orderForm: PlaceOrder = new PlaceOrder();

  constructor(
    public api: ApiService,
    public userService: UserService,
    public cartService: CartService,
    public alert: AlertService,
    public route: Router
  ) {
    this.getOrdersAmount();
  }

  /* here wo call to server and get the number of orders we have in db from this store */
  getOrdersAmount = async () => {
    this.ordersAmount = (await this.api.getApiRequest(
      'orders/getOrdersAmount'
    )) as string;
  };

  /* here we call to server and get all the orders of this user */
  getAllOrders = async () => {
    this.allOrders = (await this.api.getApiRequest(
      `orders/getOrdersByUserId?userId=${this.userService._currentUser.id}`
    )) as Array<Order>;
  };

  /* here we find in all orders array the last order of this user */
  findLastOrder = () => {
    if (this.allOrders.length > 0) {
      this.lastOrder = this.allOrders.sort(
        (a, b) =>
          new Date(b.updatedAt).getTime() - new Date(a.updatedAt).getTime()
      )[0];
    }
  };

  /* here we send a new order to DB after the user place order */
  sendNewOrder = async () => {
    let order: boolean = false;
    let orderToSend: Order = new Order();
    orderToSend.userId = this.userService._currentUser.id;
    orderToSend.cartId = this.cartService._currentCart.id;
    orderToSend.totalPrice = this.cartService.cartTotalPrice;
    orderToSend.city = this.orderForm.city;
    orderToSend.street = this.orderForm.street;
    orderToSend.deliveryDate = this.orderForm.deliveryDate;
    orderToSend.creditCard = this.orderForm.creditCard;
    order = (await this.api.postApiRequest(
      'orders/insertOrder',
      orderToSend
    )) as boolean;
    if (order == true) {
      this.orderForm = new Order();
      return true;
    } else this.alert.serverProblem('Your order was not received');
    return;
  };
}
