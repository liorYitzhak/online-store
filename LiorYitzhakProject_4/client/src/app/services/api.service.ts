import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  GlobalUrl = 'http://www.localhost:4000/';

  constructor(private httpClient: HttpClient) {}

  getApiRequest(url: string) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpClient.get(this.GlobalUrl + url).subscribe(
          (data) => {
            resolve(data);
          },
          (error) => {
            console.log('oops', error, error.error);
          }
        );
      } catch (err) {
        console.log('ERRORRR : ', err);
      }
    });
  }

  postApiRequest(url: string, obj: any) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpClient.post(this.GlobalUrl + url, obj).subscribe(
          (data) => {
            resolve(data);
          },
          (error) => {
            resolve('erroe server');
          }
        );
      } catch (err) {
        console.log('ERRORRR : ', err);
      }
    });
  }
}
