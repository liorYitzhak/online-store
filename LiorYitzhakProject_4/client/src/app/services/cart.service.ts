import { Injectable } from '@angular/core';
import { Cart } from '../modules/cart';
import {
  ProductCart,
  LiveProductCart,
  EditProduct,
} from '../modules/prodctCart';
import { Product } from '../modules/product';
import { ApiService } from './api.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  _currentCarts: Array<Cart> = new Array();
  _currentCart: Cart = new Cart();
  _liveCart: Array<LiveProductCart> = new Array();
  filterCartProducts: Array<LiveProductCart> = new Array();
  productsCart: Array<ProductCart> = new Array();
  cartTotalPrice: number = 0;
  cartStatus: string = '';
  isCartOpen: boolean = false;

  constructor(public api: ApiService, public userService: UserService) {}

  /* here we bild the user cart (the side bar) */
  buildLiveCart = () => {
    this._liveCart = new Array();
    this.productsCart.map((product) => {
      this.pushProductToLiveCart(product, product.product);
    });
    this.calculateTotalPrice(false);
    this.filterCartProducts = this._liveCart;
  };

  /* here we push the product the user added to cart to the _liveCart for drow cart */
  pushProductToLiveCart = (productCart: ProductCart, product: Product) => {
    let productToPush: LiveProductCart = new LiveProductCart();
    productToPush.quentity = productCart.quentity;
    productToPush.price = productCart.price;
    productToPush.image = product.image;
    productToPush.name = product.name;
    productToPush.id = productCart.id;
    productToPush.productId = product.id;
    this._liveCart.push(productToPush);
  };

  /* here we call to server to insert a new product to products cart */
  insertProductToProductsCart = async (product: Product, qty: number) => {
    let productToInsertCart: ProductCart = new ProductCart();
    productToInsertCart.productId = product.id;
    productToInsertCart.quentity = Number(qty);
    productToInsertCart.price = product.price * qty;
    productToInsertCart.cartId = this._currentCart.id;
    let newProduct = (await this.api.postApiRequest(
      'productsCarts/insertProduct',
      productToInsertCart
    )) as any;
    productToInsertCart.id = newProduct.id;
    this.pushProductToLiveCart(productToInsertCart, product);
  };

  /* here we update the prodct price and qty in cart */
  editProductCart = async (product: Product, qty: number) => {
    let peoductIndex = this._liveCart.findIndex(
      (item) => item.productId == product.id
    );
    this._liveCart[peoductIndex].quentity = qty;
    this._liveCart[peoductIndex].price = product.price * qty;
    let sendToServerObject: EditProduct = new EditProduct();
    sendToServerObject.id = this._liveCart[peoductIndex].id;
    sendToServerObject.price = this._liveCart[peoductIndex].price;
    sendToServerObject.quentity = qty;
    let editProduct = await this.api.postApiRequest(
      'productsCarts/updateQuentity',
      sendToServerObject
    );
  };

  /* here we delete one product from the cart */
  deleteCartProduct = async (id: number) => {
    let peoductIndex = this._liveCart.findIndex((item) => item.id == id);
    this._liveCart.splice(peoductIndex, 1);
    this.calculateTotalPrice(false);

    await this.api.getApiRequest(`productsCarts/deleteProduct?id=${id}`);
  };

  /* here we call to server and get the user cart information by userId */
  getUserCart = async (userId: number) => {
    this._currentCarts = (await this.api.getApiRequest(
      `carts/getCartsByUserId?userId=${userId}`
    )) as Array<Cart>;
    this.productsCart = new Array();
    this._liveCart = new Array();
    this.setCartStatus();
  };

  /* here we update all the info about the  user cart and open a new cart for user if dont have one */
  setCartStatus = async () => {
    if (this._currentCarts.length > 0) {
      let openCart = this._currentCarts.find((cart) => cart.isOpen == true);
      if (openCart != undefined) {
        this._currentCart = openCart;
        this.productsCart = (await this.api.getApiRequest(
          `productsCarts/getProductsByCartId?cartId=${this._currentCart.id}`
        )) as Array<ProductCart>;
        this.cartStatus = 'openCart';
        this.buildLiveCart();
        return;
      } else {
        this.openNewCart();
        this.cartStatus = 'nonOpenCart';
        this.calculateTotalPrice(false);
      }
    } else {
      this.cartStatus = 'newUser';
      this.openNewCart();
    }
  };

  /* here we do calculte to cart price. we have 2 option if  
  we in log in  page we get true agrument and do map on productsCart arr
  if we in home page we get false and do map on _liveCart arr  
  */
  calculateTotalPrice = (isLogInFage: boolean) => {
    this.cartTotalPrice = 0;
    if (isLogInFage) {
      this.productsCart.map((prodct) => {
        this.cartTotalPrice += prodct.price * prodct.quentity;
      });
    } else {
      this._liveCart.map((prodct) => {
        this.cartTotalPrice += Number(prodct.price);
      });
    }
  };

  /* here we open new cart for user as no have a open cart */
  openNewCart = async () => {
    if (this.userService.userInHomePage) {
      this.isCartOpen = true;
      let cart: Cart = new Cart();
      cart.userId = this.userService._currentUser.id;
      cart.isOpen = true;
      this._currentCart = (await this.api.postApiRequest(
        'carts/insertCart',
        cart
      )) as Cart;
    }
  };

  /* here we close the cart after the user finsh the order */
  markCartAsClose = async () => {
    this._currentCart.isOpen = false;
    await this.api.postApiRequest('carts/updateCart', this._currentCart);
  };

  /* here we delete all products from crat */
  clearCart = async () => {
    this._liveCart = new Array();
    this.calculateTotalPrice(false);
    await this.api.getApiRequest(
      `productsCarts/deleteProductsByCartId?id=${this._currentCart.id}`
    );
  };
}
