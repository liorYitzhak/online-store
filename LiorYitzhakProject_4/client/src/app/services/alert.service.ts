import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ExportFileService } from './export-file.service';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  constructor(public route: Router, public exportService: ExportFileService) {}

  /* here we do alert When there is a problem */
  serverProblem = (text: string) => {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: text,
      footer:
        'We are sorry there is a problem with the server. Please try again later',
    });
  };

  /* here we do alert When Action performed successfully */
  successMessage = (text: string) => {
    Swal.fire({
      position: 'top',
      icon: 'success',
      title: text,
      showConfirmButton: false,
      timer: 2300,
    });
  };

  /* here we do alert When user place order success */
  orderPlacedOk = () => {
    Swal.fire({
      title: 'Your purchase was made successfully!',
      text: 'Your products will be packed for shipment very soon.',
      allowOutsideClick: false,
      animation: true,
      showCancelButton: true,
      cancelButtonText: 'Download receipt ',
      confirmButtonText: 'Confirmation',
      cancelButtonColor: '#a4d668',
      confirmButtonColor: '#e2ad28',
      imageUrl: '../../assets/images/moving.png',
      imageWidth: 500,
      imageHeight: 300,
      imageAlt: 'Custom image',
    }).then((result) => {
      if (result.isConfirmed == false) this.exportService.exportElmToExcel();
      this.route.navigate(['/login']);
    });
  };
}
